﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.App_Code
{
    public static class CustomerHelpers
    {

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        {
            var result =
                from e in Enum.GetValues(typeof(TEnum)).Cast<TEnum>()
                select new
                {
                    Id = (int)Enum.Parse(typeof(TEnum), e.ToString()),
                    Name = e.ToString()
                };
            return new SelectList(result, "Id", "Name", enumObj);
        }

    }


 


}