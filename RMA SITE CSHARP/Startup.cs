﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RMA_SITE.Startup))]
namespace RMA_SITE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
