using System.Web.Routing;
using RestfulRouting;
using RMA_SITE.Controllers;

[assembly: WebActivator.PreApplicationStartMethod(typeof(RMA_SITE.Routes), "Start")]

namespace RMA_SITE
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.DebugRoute("routedebug");


        }

        public static void Start()
        {
            var routes = RouteTable.Routes;
            routes.MapRoutes<Routes>();
            
        }
    }
}