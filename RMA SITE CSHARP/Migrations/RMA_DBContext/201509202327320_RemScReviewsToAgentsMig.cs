namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemScReviewsToAgentsMig : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScReviewModels", "Agent_ID", "dbo.Agents");
            DropIndex("dbo.ScReviewModels", new[] { "Agent_ID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.ScReviewModels", "Agent_ID");
            AddForeignKey("dbo.ScReviewModels", "Agent_ID", "dbo.Agents", "ID");
        }
    }
}
