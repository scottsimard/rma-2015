namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScReviewMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScReviewModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        RMA_ID = c.Int(nullable: false),
                        review_summary = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ScReviewModels");
        }
    }
}
