namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProFieldModelMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProFields",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        field_type = c.Int(nullable: false),
                        RMA_ID = c.Int(nullable: false),
                        field_value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Agents", "pro_member", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Agents", "pro_member");
            DropTable("dbo.ProFields");
        }
    }
}
