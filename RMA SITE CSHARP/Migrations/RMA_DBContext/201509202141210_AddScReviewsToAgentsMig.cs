namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScReviewsToAgentsMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ScReviewModels", "name", c => c.String(nullable: false));
            AlterColumn("dbo.ScReviewModels", "email", c => c.String(nullable: false));
            CreateIndex("dbo.ScReviewModels", "Agent_ID");
            AddForeignKey("dbo.ScReviewModels", "Agent_ID", "dbo.Agents", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScReviewModels", "Agent_ID", "dbo.Agents");
            DropIndex("dbo.ScReviewModels", new[] { "Agent_ID" });
            AlterColumn("dbo.ScReviewModels", "email", c => c.String());
            AlterColumn("dbo.ScReviewModels", "name", c => c.String());
        }
    }
}
