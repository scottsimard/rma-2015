namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScReviewFieldsMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScReviewModels", "agent_id", c => c.Int());
            AddColumn("dbo.ScReviewModels", "overall_rating", c => c.Int(nullable: false));
            AddColumn("dbo.ScReviewModels", "friendliness_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "understanding_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "professionalism_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "experience_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "successful", c => c.Int());
            AddColumn("dbo.ScReviewModels", "transaction_type_id", c => c.Int(nullable: false));
            AddColumn("dbo.ScReviewModels", "property_type_id", c => c.Int(nullable: false));
            AddColumn("dbo.ScReviewModels", "comment", c => c.String());
            AddColumn("dbo.ScReviewModels", "helpfulness_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "responsiveness_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "attitude_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "created_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.ScReviewModels", "updated_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.ScReviewModels", "verified_reply", c => c.String());
            AddColumn("dbo.ScReviewModels", "verification_key", c => c.String());
            AddColumn("dbo.ScReviewModels", "market_research_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "home_prep_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "handelling_claims_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "mortgage_guidance_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "website_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "service_rating", c => c.Int());
            AddColumn("dbo.ScReviewModels", "show_home", c => c.String());
            AddColumn("dbo.ScReviewModels", "recommend_agent", c => c.String());
            AddColumn("dbo.ScReviewModels", "name", c => c.String());
            AddColumn("dbo.ScReviewModels", "email", c => c.String());
            AddColumn("dbo.ScReviewModels", "receive_review_notification", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScReviewModels", "receive_review_notification");
            DropColumn("dbo.ScReviewModels", "email");
            DropColumn("dbo.ScReviewModels", "name");
            DropColumn("dbo.ScReviewModels", "recommend_agent");
            DropColumn("dbo.ScReviewModels", "show_home");
            DropColumn("dbo.ScReviewModels", "service_rating");
            DropColumn("dbo.ScReviewModels", "website_rating");
            DropColumn("dbo.ScReviewModels", "mortgage_guidance_rating");
            DropColumn("dbo.ScReviewModels", "handelling_claims_rating");
            DropColumn("dbo.ScReviewModels", "home_prep_rating");
            DropColumn("dbo.ScReviewModels", "market_research_rating");
            DropColumn("dbo.ScReviewModels", "verification_key");
            DropColumn("dbo.ScReviewModels", "verified_reply");
            DropColumn("dbo.ScReviewModels", "updated_at");
            DropColumn("dbo.ScReviewModels", "created_at");
            DropColumn("dbo.ScReviewModels", "attitude_rating");
            DropColumn("dbo.ScReviewModels", "responsiveness_rating");
            DropColumn("dbo.ScReviewModels", "helpfulness_rating");
            DropColumn("dbo.ScReviewModels", "comment");
            DropColumn("dbo.ScReviewModels", "property_type_id");
            DropColumn("dbo.ScReviewModels", "transaction_type_id");
            DropColumn("dbo.ScReviewModels", "successful");
            DropColumn("dbo.ScReviewModels", "experience_rating");
            DropColumn("dbo.ScReviewModels", "professionalism_rating");
            DropColumn("dbo.ScReviewModels", "understanding_rating");
            DropColumn("dbo.ScReviewModels", "friendliness_rating");
            DropColumn("dbo.ScReviewModels", "overall_rating");
            DropColumn("dbo.ScReviewModels", "agent_id");
        }
    }
}
