namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeRMAIDtypeMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Agents", "RMA_ID", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Agents", "RMA_ID", c => c.String());
        }
    }
}
