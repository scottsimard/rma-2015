namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemReviewsFromAgentsMig : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reviews", "Agent_ID", "dbo.Agents");
            DropIndex("dbo.Reviews", new[] { "Agent_ID" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Reviews", "Agent_ID");
            AddForeignKey("dbo.Reviews", "Agent_ID", "dbo.Agents", "ID");
        }
    }
}
