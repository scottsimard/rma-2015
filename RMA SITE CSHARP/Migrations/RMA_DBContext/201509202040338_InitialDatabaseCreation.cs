namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabaseCreation : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Agents",
            //    c => new
            //        {
            //            ID = c.Int(nullable: false, identity: true),
            //            RMA_ID = c.String(),
            //            photo_content = c.Binary(),
            //            agent_type = c.String(),
            //            first_name = c.String(),
            //            last_name = c.String(),
            //            phone_number = c.String(),
            //            cell_number = c.String(),
            //            fax_number = c.String(),
            //            address1 = c.String(),
            //            address2 = c.String(),
            //            city = c.String(),
            //            prov_state = c.String(),
            //            country = c.String(),
            //            email = c.String(),
            //            web_site = c.String(),
            //            company_name = c.String(),
            //            created_at = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //            updated_at = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //            average_overall_rating_cache = c.Single(nullable: false),
            //            cumulative_rating = c.Single(nullable: false),
            //            average_overall_ranking = c.Single(nullable: false),
            //            total_review_count = c.Int(nullable: false),
            //            active = c.Int(nullable: false),
            //            photo_filename = c.String(),
            //            photo_link = c.String(),
            //        })
            //    .PrimaryKey(t => t.ID);
            
            //CreateTable(
            //    "dbo.Reviews",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            RMA_ID = c.Int(nullable: false),
            //            review_summary = c.String(),
            //            agent_id = c.Int(),
            //            overall_rating = c.Int(nullable: false),
            //            friendliness_rating = c.Int(),
            //            understanding_rating = c.Int(),
            //            professionalism_rating = c.Int(),
            //            experience_rating = c.Int(),
            //            successful = c.Int(),
            //            transaction_type_id = c.Int(nullable: false),
            //            property_type_id = c.Int(nullable: false),
            //            comment = c.String(),
            //            helpfulness_rating = c.Int(),
            //            responsiveness_rating = c.Int(),
            //            attitude_rating = c.Int(),
            //            created_at = c.DateTime(precision: 7, storeType: "datetime2"),
            //            updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
            //            verified_reply = c.String(),
            //            verification_key = c.String(),
            //            market_research_rating = c.Int(),
            //            home_prep_rating = c.Int(),
            //            handelling_claims_rating = c.Int(),
            //            mortgage_guidance_rating = c.Int(),
            //            website_rating = c.Int(),
            //            service_rating = c.Int(),
            //            show_home = c.String(),
            //            recommend_agent = c.String(),
            //            name = c.String(),
            //            email = c.String(),
            //            receive_review_notification = c.String(),
            //            Agent_ID = c.Int(),
            //        })
            //    .PrimaryKey(t => t.id)
            //    .ForeignKey("dbo.Agents", t => t.Agent_ID)
            //    .Index(t => t.Agent_ID);
            
            //CreateTable(
            //    "dbo.AgentTypes",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            type_name = c.String(),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.Cities",
            //    c => new
            //        {
            //            ID = c.String(nullable: false, maxLength: 128),
            //            created_at = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //            updated_at = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
            //            name = c.String(),
            //            prov_state = c.String(),
            //            description = c.String(),
            //            desc_realty = c.String(),
            //            desc_mortgage = c.String(),
            //            desc_insurance = c.String(),
            //            agent_count = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.ID);
            
            //CreateTable(
            //    "dbo.DataImporterModels",
            //    c => new
            //        {
            //            ID = c.Int(nullable: false, identity: true),
            //        })
            //    .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "Agent_ID", "dbo.Agents");
            DropIndex("dbo.Reviews", new[] { "Agent_ID" });
            DropTable("dbo.DataImporterModels");
            DropTable("dbo.Cities");
            DropTable("dbo.AgentTypes");
            DropTable("dbo.Reviews");
            DropTable("dbo.Agents");
        }
    }
}
