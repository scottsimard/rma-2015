namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeRequiredAgentFieldsMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Agents", "first_name", c => c.String(nullable: false));
            AlterColumn("dbo.Agents", "last_name", c => c.String(nullable: false));
            AlterColumn("dbo.Agents", "city", c => c.String(nullable: false));
            AlterColumn("dbo.Agents", "prov_state", c => c.String(nullable: false));
            AlterColumn("dbo.Agents", "country", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Agents", "country", c => c.String());
            AlterColumn("dbo.Agents", "prov_state", c => c.String());
            AlterColumn("dbo.Agents", "city", c => c.String());
            AlterColumn("dbo.Agents", "last_name", c => c.String());
            AlterColumn("dbo.Agents", "first_name", c => c.String());
        }
    }
}
