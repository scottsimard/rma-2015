namespace RMA_SITE.Migrations.RMA_DBContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNullValuesReviewModelMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "understanding_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "professionalism_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "successful", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "responsiveness_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "market_research_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "home_prep_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "website_rating", c => c.Int(nullable: false));
            AlterColumn("dbo.Reviews", "service_rating", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "service_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "website_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "home_prep_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "market_research_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "responsiveness_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "successful", c => c.Int());
            AlterColumn("dbo.Reviews", "professionalism_rating", c => c.Int());
            AlterColumn("dbo.Reviews", "understanding_rating", c => c.Int());
        }
    }
}
