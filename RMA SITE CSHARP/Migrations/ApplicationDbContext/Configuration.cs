namespace RMA_SITE.Migrations.ApplicationDbContext
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RMA_SITE.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\ApplicationDbContext";
        }

        protected override void Seed(RMA_SITE.Models.ApplicationDbContext context)
        {

            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "admin" });   
            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "public" });
            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "member" });
        
            var role = context.Roles.SingleOrDefault(m => m.Name == "admin");


            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("admin");

            ApplicationUser new_user = new ApplicationUser
            {
                Email = "admin@admin.com",
                EmailConfirmed = true,
                first_name = "Scott",
                last_name = "Simard",
                UserName = "admin",
                PasswordHash = password,
                PhoneNumber = "2506172656"

            };
                       
            new_user.Roles.Add(new IdentityUserRole { RoleId = role.Id });

            context.Users.AddOrUpdate(u => u.UserName, new_user);
                        

            context.SaveChanges();

        }
    }
}
