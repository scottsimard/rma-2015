﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;

namespace RMA_SITE.Controllers
{
    public class ScReviewModelsController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: ScReviewModels
        public ActionResult Index()
        {
            return View(db.ScReviews.ToList());
        }

        // GET: ScReviewModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScReviewModel scReviewModel = db.ScReviews.Find(id);
            if (scReviewModel == null)
            {
                return HttpNotFound();
            }
            return View(scReviewModel);
        }

        // GET: ScReviewModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ScReviewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] ScReviewModel scReviewModel)
        {
            if (ModelState.IsValid)
            {
                db.ScReviews.Add(scReviewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(scReviewModel);
        }

        // GET: ScReviewModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScReviewModel scReviewModel = db.ScReviews.Find(id);
            if (scReviewModel == null)
            {
                return HttpNotFound();
            }
            return View(scReviewModel);
        }

        // POST: ScReviewModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] ScReviewModel scReviewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(scReviewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(scReviewModel);
        }

        // GET: ScReviewModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScReviewModel scReviewModel = db.ScReviews.Find(id);
            if (scReviewModel == null)
            {
                return HttpNotFound();
            }
            return View(scReviewModel);
        }

        // POST: ScReviewModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ScReviewModel scReviewModel = db.ScReviews.Find(id);
            db.ScReviews.Remove(scReviewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
