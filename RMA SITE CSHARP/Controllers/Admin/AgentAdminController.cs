﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;
using PagedList;

namespace RMA_SITE.Controllers.Admin
{
    [Authorize(Roles = "admin")]
    public class AgentAdminController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: AgentAdmin
        public ActionResult Index(int? page)
        {

            IEnumerable<RMA_SITE.Models.Agent> agents = db.Agents.ToList();
            agents = agents.OrderByDescending(s => s.average_overall_rating_cache);

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));


        }


        // GET: AgentAdmin/Search
        public ActionResult Search(string param, string v, string sortorder, int? page)
        {


            ViewBag.Message = "Search Results.";

            if (param == "#") { param = ""; }
            ViewBag.CurrentFilter = param;
            if (ViewBag.CurrentFilter == "") { ViewBag.CurrentFilter = "#"; }

            ViewBag.CurrentSort = sortorder;


            var search_terms = param.Split(' ').ToList();

            List<Agent> agents = new List<Agent>();

            foreach (string search_term in search_terms)
            {

                var curr_agents = db.Agents.Where(s => (s.first_name.Contains(search_term) || s.last_name.Contains(search_term) || s.city.Contains(search_term)));

                agents = agents.Union(curr_agents).ToList();

            }



            var qagents = agents.AsQueryable();

            if (v == "l")
            {
                return View("Index", qagents);
            }
            else
            {
                switch (sortorder)
                {
                    case "name_desc":
                        qagents = qagents.OrderByDescending(s => s.first_name);
                        break;

                }


                int pageSize = 10;
                int pageNumber = (page ?? 1);
                return View("Index", qagents.ToPagedList(pageNumber, pageSize));
                //return View(agents.ToPagedList(pageNumber, pageSize));
            }

        }


        // GET: AgentAdmin/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // GET: AgentAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AgentAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,RMA_ID,photo_content,agent_type,first_name,last_name,phone_number,cell_number,fax_number,address1,address2,city,prov_state,country,email,web_site,company_name,created_at,updated_at,average_overall_rating_cache,average_overall_ranking,total_review_count,active,photo_filename,photo_link")] Agent agent)
        {
            if (ModelState.IsValid)
            {
                db.Agents.Add(agent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(agent);
        }

        // GET: AgentAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // POST: AgentAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,RMA_ID,photo_content,agent_type,first_name,last_name,phone_number,cell_number,fax_number,address1,address2,city,prov_state,country,email,web_site,company_name,created_at,updated_at,average_overall_rating_cache,average_overall_ranking,total_review_count,active,photo_filename,photo_link")] Agent agent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(agent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(agent);
        }

        // GET: AgentAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // POST: AgentAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Agent agent = db.Agents.Find(id);
            db.Agents.Remove(agent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
