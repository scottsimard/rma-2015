﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMA_SITE.Controllers.Admin
{
    [Authorize(Roles="admin")]
    public class AdminController : Controller
    {
        // GET: Admin
     
        public ActionResult Index()
        {
            return View();
        }
    }
}