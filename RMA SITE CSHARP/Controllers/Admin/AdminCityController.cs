﻿using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers.Admin
{
    [Authorize(Roles = "admin")]
    public class AdminCityController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: AdminCity
        public ActionResult Index(int? selected_city_id)
        {

            AdminCityViewModel t_admin_city_view_model = new AdminCityViewModel();

            var city_list = db.Cities ;
            List<SelectListItem> items = new List<SelectListItem>();

            var sorted_cities = city_list.OrderBy(s => s.name);

            foreach (City t in sorted_cities)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.name  + ", " + t.prov_state ;
                s.Value = Convert.ToString(t.ID);
                if (selected_city_id != null)
                {
                    if (selected_city_id == Convert.ToInt16(t.ID)) 
                    {
                        s.Selected = true;
                        t_admin_city_view_model.selected_city = t;
                        t_admin_city_view_model.selected_city_id = Convert.ToInt16(selected_city_id);
                    }
                }

                items.Add(s);
            }

            ViewBag.city_list = items;
            if (selected_city_id == null)
            {
                selected_city_id = 0;
            }
            ViewBag.city_id = selected_city_id;

            return View(t_admin_city_view_model);
        }

        // POST: AdminCity/Update/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateDescriptions(string city_id, string city_description, string city_desc_realty, string city_desc_insurance, string city_desc_mortgage)
        {
            City city = new City();


            var city_list = db.Cities;

            city = db.Cities.Where(m => m.ID == city_id).FirstOrDefault();

            city.description = city_description;
            city.desc_realty = city_desc_realty;
            city.desc_insurance = city_desc_insurance;
            city.desc_mortgage = city_desc_mortgage;

            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { selected_city_id = city_id});
             
            }
            return RedirectToAction("Index", new { selected_city_id = city_id });
        }

    }
}