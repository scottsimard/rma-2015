﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers.Admin
{
    [Authorize(Roles = "admin")]
    public class AdminSettingsController : Controller
    {
        // GET: AdminSettings
        public ActionResult Index()
        {
            return View();
        }
    }
}