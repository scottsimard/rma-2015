﻿using PagedList;
using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers
{
    public class HomeController : Controller
    {

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
        int pageSize = 12;

        public ActionResult Index(string id="")
        {
            ViewBag.Title = "Rate-My-Agent.com Ratings and Reviews of Real Estate Agents, Insurance, and Mortgage Brokers";
            ViewBag.MetaDescription = "Ratings and reviews at Rate-My-Agent.com.  Rate or review your real estate agent, insurance, or mortgage brokers.";

            if (id != "")
            {
                return RedirectToAction("Index", "Agents");

            }

            var DistinctItems = db.Cities.GroupBy(x => x.name).Select(y => y.FirstOrDefault()).ToList();

            DistinctItems =  DistinctItems.OrderByDescending(m => m.agent_count).ToList();


            var top_twenty = DistinctItems.Take(20).ToList();

            HomeIndexViewModelTopTwenty view_model_top_twenty = new HomeIndexViewModelTopTwenty();
            view_model_top_twenty.top_twenty_cities = top_twenty;

            return View("Index", view_model_top_twenty);
        }

        public ActionResult About()
        {
            ViewBag.Title = "Our Story at Rate - My - Agent.com: Why Reviews and Ratings Matter";
            ViewBag.MetaDescription = "Ratings and reviews matter and Rate My Agent was created to help people find mortgage, insurance, and real estate professionals who take the best care of their clients.  Of course, we also want to help save people from the agony caused by a negligent agent.";
            ViewBag.Message = "About";

            return View();
        }

        public ActionResult Advertising()
        {
            ViewBag.Title = "Advertise at Rate-My-Agent.com | Online advertising for Mortgage Brokers, Insurance Agents, Realtors, and all real estate professionals.";
            ViewBag.MetaDescription = "Online advertising at Rate-My-Agent.com. Mortgage Brokers, Insurance Agents, Realtors, and all real estate professionals  advertise at Rate-My-Agent.com to reach qualified clients.";
            ViewBag.Message = "Advertising";

            return View();
        }


        public ActionResult RealEstate(int? page)
        {
            ViewBag.Title = "Real Estate agent ratings and reviews.";
            ViewBag.Message = "Top Real Estate Agent Reviews and Ratings";
            ViewBag.MetaDescription = "Top real estate agent ratings and reviews. Rate-My-Agent.com makes it easy to rate and review your real estate agent.";
            ViewBag.ControllerAction = "RealEstate";

            var agents = db.Agents.Where(s => (s.agent_type.Contains("Real Estate")));
            agents = agents.OrderByDescending(s => s.cumulative_rating); //.ThenByDescending(t => t.total_review_count);

           
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Insurance(int? page)
        {
            ViewBag.Title = "Insurance agent ratings and reviews.";
            ViewBag.Message = "Top Insurance Agent Reviews and Ratings";
            ViewBag.MetaDescription = "Top insurance agent ratings and reviews.  Rate - My - Agent.com makes it easy to rate and review your insurance agent.";
            ViewBag.ControllerName = "Insurance";

            var agents = db.Agents.Where(s => (s.agent_type.Contains("Insurance")));
            agents = agents.OrderByDescending(s => s.cumulative_rating); //.ThenByDescending(t => t.total_review_count);


            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Mortgage(int? page)
        {
            ViewBag.Title = "Mortgage Broker ratings and reviews.";
            ViewBag.Message = "Top Mortgage Broker Reviews and Ratings";
            ViewBag.MetaDescription = "Top mortgage broker ratings and reviews.  Rate-My-Agent.com makes it easy to rate and review your mortgage broker.";
            ViewBag.ControllerName = "Mortgage";

            var agents = db.Agents.Where(s => (s.agent_type.Contains("Mortgage")));
            agents = agents.OrderByDescending(s => s.cumulative_rating); //.ThenByDescending (t => t.total_review_count);
            
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));            

        }

        

        public ActionResult Contact()
        {
            ViewBag.Title = "Contact Rate-My-Agent.com by email.";
            ViewBag.MetaDescription = "Contact Rate-My-Agent.com (RMA) by email.";
            ViewBag.Message = "Contact";

            return View();
        }
    }
}