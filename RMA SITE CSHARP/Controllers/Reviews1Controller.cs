﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;

namespace RMA_SITE.Controllers
{
    public class Reviews1Controller : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: Reviews1
        public ActionResult Index()
        {
            return View(db.Reviews.ToList());
        }

        // GET: Reviews1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // GET: Reviews1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reviews1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Reviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(review);
        }

        // GET: Reviews1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // POST: Reviews1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Entry(review).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(review);
        }

        // GET: Reviews1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        // POST: Reviews1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Review review = db.Reviews.Find(id);
            db.Reviews.Remove(review);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
