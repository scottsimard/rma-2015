﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Diagnostics;

namespace RMA_SITE.Controllers
{
    [Authorize(Roles = "admin")]
    public class DataImportController : Controller
    {

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
        private DataImporterModel data_importer = new DataImporterModel();

        // GET: DataImport
        public ActionResult Index()
        {
            return View();
        }

        // GET: DataImport/ClearTables
        public ActionResult ClearTables()
        {
            return View();
        }

        // GET: DataImport/ClearProceedAgents
        public ActionResult ClearProceedAgents()
        {

            string sql = "TRUNCATE TABLE Agents;";
            int result = db.Database.ExecuteSqlCommand(sql);
            
            return View("ClearCompleted");
        }

        // GET: DataImport/ClearProceedReviews
        public ActionResult ClearProceedReviews()
        {

            string sql = "TRUNCATE TABLE Reviews;";
            int result = db.Database.ExecuteSqlCommand(sql);

            return View("ClearCompleted");
        }

        // GET: DataImport/ClearProceedCities
        public ActionResult ClearProceedCities()
        {

            string sql = "TRUNCATE TABLE Cities;";
            int result = db.Database.ExecuteSqlCommand(sql);

            return View("ClearCompleted");
        }

        public ActionResult UploadFile() //int? chunk, string name)
        {
                  
         
             return View();
        }

        // GET: DataImport/ClearCompleted
        public ActionResult ClearCompleted()
        {

    
            return View();
        }

        public ActionResult LoadImportFile_Agents()
        {
            string input_file = Server.MapPath(@"~/App_Data/agents_import.csv");

            string str_report = "";

            using (StreamReader CsvReader = new StreamReader(input_file))
            {
                string inputLine = "";
             
                while ((inputLine = CsvReader.ReadLine()) != null)
                {
                    inputLine = inputLine.Replace(",", "&#44;");
                    inputLine = inputLine.Replace("\"", "");
                    inputLine = inputLine.Replace("||||", ",");

                    var line_values = inputLine.Split(',');

                    int i_counter = 0;
                    foreach (string t_value in line_values)
                    {
                        line_values[i_counter] = t_value.Replace("&#44;", ",");
                        i_counter++;
                    }

                    if (line_values.Count() > 20)
                    {
                        try
                        {
                            Agent temp_agent = new Agent();

                            temp_agent.RMA_ID = Convert.ToInt32(line_values[0]);
                            temp_agent.photo_filename = line_values[1];
                            temp_agent.agent_type = line_values[2];
                            temp_agent.photo_link = line_values[3];
                            temp_agent.first_name = line_values[4];
                            temp_agent.last_name = line_values[5];

                            temp_agent.phone_number = line_values[6];
                            temp_agent.cell_number = line_values[7];
                            temp_agent.fax_number = line_values[8];
                            temp_agent.address1 = line_values[9];
                            temp_agent.address2 = line_values[10];
                            temp_agent.city = line_values[11];
                            temp_agent.prov_state = line_values[12];
                            temp_agent.country = line_values[13];

                            temp_agent.email = line_values[14];
                            temp_agent.web_site = line_values[15];

                            DateTime parsed_created;
                            DateTime parsed_updated;
                            DateTime.TryParse(line_values[16], out parsed_created);
                            DateTime.TryParse(line_values[17], out parsed_updated);
                            temp_agent.created_at = parsed_created;
                            temp_agent.updated_at = parsed_updated;

                            temp_agent.company_name = line_values[18];

                            float parsed_rating; float.TryParse(line_values[19], out parsed_rating);
                            temp_agent.average_overall_rating_cache = parsed_rating;
                            
                            int parsed;
                            int.TryParse(line_values[20], out parsed); temp_agent.active = parsed;
                       
                            db.Agents.Add(temp_agent);
                            db.SaveChanges();
                        }
                        catch { }
                    }
          
                }
               
                CsvReader.Close();
                                   
            }

            ViewBag.FileContents = str_report;

            return View();
        }

        public ActionResult LoadImportFile_Reviews()
        {
            string input_file = Server.MapPath(@"~/App_Data/reviews_import.csv");
            string str_report = "";

            using (StreamReader CsvReader = new StreamReader(input_file))
            {
                string inputLine = "";

                while ((inputLine = CsvReader.ReadLine()) != null)
                {
                    inputLine = inputLine.Replace(",", "&#44;");
                    inputLine = inputLine.Replace("\"", "");
                    inputLine = inputLine.Replace("||||", ",");
                    
                    var line_values = inputLine.Split(',');

                    int i_counter = 0;
                    foreach (string t_value in line_values)
                    {
                        line_values[i_counter] = t_value.Replace("&#44;", ",");
                        i_counter++;
                    }
                    
                    if (line_values.Count() > 20)
                    {
                        try
                        {
                            Review temp_review = new Review();

                            int parsed;

                            int.TryParse(line_values[0], out parsed); temp_review.RMA_ID = parsed;
                            int.TryParse(line_values[1], out parsed); temp_review.agent_id = parsed;
                            int.TryParse(line_values[2], out parsed); temp_review.overall_rating = parsed;
                            int.TryParse(line_values[3], out parsed); temp_review.friendliness_rating = parsed;
                            int.TryParse(line_values[4], out parsed); temp_review.understanding_rating = parsed;
                            int.TryParse(line_values[5], out parsed); temp_review.experience_rating = parsed;
                            int.TryParse(line_values[6], out parsed); temp_review.successful = parsed;
                            int.TryParse(line_values[7], out parsed); temp_review.transaction_type_id = parsed;
                            int.TryParse(line_values[8], out parsed); temp_review.experience_rating = parsed;
                            int.TryParse(line_values[9], out parsed); temp_review.property_type_id = parsed;

                            temp_review.comment = line_values[10];
                            temp_review.review_summary = line_values[10];

                            int.TryParse(line_values[11], out parsed); temp_review.helpfulness_rating = parsed;
                            int.TryParse(line_values[12], out parsed); temp_review.responsiveness_rating = parsed;
                            int.TryParse(line_values[13], out parsed); temp_review.attitude_rating = parsed;

                            DateTime parsed_created;
                            DateTime parsed_updated;
                            DateTime.TryParse(line_values[14], out parsed_created);
                            DateTime.TryParse(line_values[15], out parsed_updated);

                            temp_review.created_at = parsed_created;
                            temp_review.updated_at = parsed_updated;

                            temp_review.verified_reply = line_values[16];
                            temp_review.verification_key = line_values[17];

                            int.TryParse(line_values[18], out parsed); temp_review.market_research_rating = parsed;
                            int.TryParse(line_values[19], out parsed); temp_review.home_prep_rating = parsed;
                            int.TryParse(line_values[20], out parsed); temp_review.handelling_claims_rating = parsed;
                            int.TryParse(line_values[21], out parsed); temp_review.mortgage_guidance_rating = parsed;
                            int.TryParse(line_values[22], out parsed); temp_review.website_rating = parsed;
                            int.TryParse(line_values[23], out parsed); temp_review.service_rating = parsed;

                            temp_review.show_home = line_values[24];
                            temp_review.recommend_agent = line_values[25];

                            temp_review.name = line_values[26];
                            temp_review.email = line_values[27];
                            temp_review.receive_review_notification = line_values[28];

                            db.Reviews.Add(temp_review);
                            db.SaveChanges();
                        }
                        catch { }
                    }
                }
                CsvReader.Close();
            }

            ViewBag.FileContents = str_report;

            return View();
        }
        
        public string[] process_import_line(string inputLine)
        {
            inputLine = inputLine.Replace(",", "&#44;");
            inputLine = inputLine.Replace("\"", "");
            inputLine = inputLine.Replace("||||", ",");

            var line_values = inputLine.Split(',');

            int i_counter = 0;
            foreach (string t_value in line_values)
            {
                line_values[i_counter] = t_value.Replace("&#44;", ",");
                i_counter++;
            }
            return line_values;
        }


        public ActionResult LoadImportFile_Cities()
        {
            string input_file = Server.MapPath(@"~/App_Data/cities_import.csv");

            using (StreamReader CsvReader = new StreamReader(input_file))
            {
                string inputLine = "";

                while ((inputLine = CsvReader.ReadLine()) != null)
                {
                    var line_values = process_import_line(inputLine);

                    if (line_values.Count() == 9)
                    {
                        try
                        {
                            City temp_city = new City();

                            temp_city.ID = line_values[0];

                            temp_city.name = line_values[1];
                            temp_city.description  = line_values[2];

                            DateTime parsed_created;
                            DateTime parsed_updated;
                            DateTime.TryParse(line_values[3], out parsed_created);
                            DateTime.TryParse(line_values[4], out parsed_updated);

                            temp_city.updated_at = parsed_updated;
                            temp_city.created_at = parsed_created;

                            temp_city.prov_state  = line_values[5];
                            temp_city.desc_realty  = line_values[6];
                            temp_city.desc_mortgage = line_values[7];
                            temp_city.desc_insurance  = line_values[8];
                            
                            db.Cities.Add(temp_city);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            Debug.Fail("Unhandled error: " + ex);
                        }
                    }
                }
                CsvReader.Close();
            }

            return View("ImportComplete");
        }



    }
}