﻿using PagedList;
using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace RMA_SITE.Controllers
{


    public class CityController : Controller
    {
        string Capitalize(string source_string)
        {
            if (source_string.Length > 1)
            {
                return source_string.ToUpper().Substring(0, 1) + source_string.ToLower().Substring(1);
            }
            else
            {
                return source_string.ToUpper();
            }
        }

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
        int pageSize = 12;

        // GET: City
        public ActionResult Index(string city, string prov, int? page)
        {
            ViewBag.Title = Capitalize(city) + ", " + prov.ToUpper() + ", Ratings and Reviews";
            ViewBag.MetaDescription = "Agent ratings and reviews in " + Capitalize(city) + ", " + prov.ToUpper();
            ViewBag.ControllerName = "Index";

            ViewBag.City = city;
            ViewBag.Province = prov;
            
            City selected_city = db.Cities.Where(g => (g.name == city & g.prov_state == prov)).FirstOrDefault();
            if (selected_city != null) { ViewBag.CityPageContent = selected_city.description; }
            if(ViewBag.CityPageContent == "NULL") { ViewBag.CityPageContent = ""; }

            ViewBag.Message = "Top " + Capitalize(city) + ", " + prov.ToUpperInvariant() + " Agents Ratings and Reviews";

            var agents = db.Agents.Where(s => (s.city.Contains(city)));
            agents = agents.OrderByDescending(s => s.cumulative_rating);


            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));

        }

     public ActionResult Mortgage(string city, string prov, int? page)
        {
            ViewBag.Title = Capitalize(city) + ", " + prov.ToUpper() + ", Mortgage Broker Ratings and Reviews";
            ViewBag.MetaDescription = "Mortgage Broker ratings and reviews in " + Capitalize(city) + ", " + prov.ToUpper();
            ViewBag.ControllerName = "Mortgage";

            ViewBag.City = city;
            ViewBag.Province = prov;

            City selected_city = db.Cities.Where(g => (g.name == city & g.prov_state == prov)).FirstOrDefault();
            if (selected_city != null) { ViewBag.CityPageContent = selected_city.desc_mortgage; }
            if (ViewBag.CityPageContent == null) { ViewBag.CityPageContent = ""; }


            ViewBag.Message = "Top " + Capitalize(city) + ", " + prov.ToUpperInvariant() + " Mortgage Brokers - Ratings and Reviews";

            var agents = db.Agents.Where(s => (s.city.Contains(city)) & (s.agent_type.Contains("Mortgage")));
            agents = agents.OrderByDescending(s => s.cumulative_rating);

         
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Insurance(string city, string prov, int? page)
        {
            ViewBag.Title = Capitalize(city) + ", " + prov.ToUpper() + ", Insurance Agent Ratings and Reviews";
            ViewBag.MetaDescription = "Insurance Agent ratings and reviews in " + Capitalize(city) + ", " + prov.ToUpper();
            ViewBag.ControllerName = "Insurance";

            ViewBag.City = city;
            ViewBag.Province = prov;

            City selected_city = db.Cities.Where(g => (g.name == city & g.prov_state == prov)).FirstOrDefault();
            if (selected_city != null) { ViewBag.CityPageContent = selected_city.desc_insurance; }
            if (ViewBag.CityPageContent == null) { ViewBag.CityPageContent = ""; }

            ViewBag.Message = "Top " + Capitalize(city) + ", " + prov.ToUpperInvariant() + " Insurance Agents - Ratings and Reviews";

            var agents = db.Agents.Where(s => (s.city.Contains(city)) & (s.agent_type.Contains("Insurance")));
            agents = agents.OrderByDescending(s => s.cumulative_rating);

           
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult RealEstate(string city, string prov, int? page)
        {
            ViewBag.Title = Capitalize(city) + ", " + prov.ToUpper() + ", Real Estate Agent Ratings and Reviews";
            ViewBag.MetaDescription = "Real Estate Agent ratings and reviews in " + Capitalize(city) + ", " + prov.ToUpper();
            ViewBag.ControllerName = "RealEstate";

            ViewBag.City = city;
            ViewBag.Province = prov;

            City selected_city = db.Cities.Where(g => (g.name == city & g.prov_state == prov)).FirstOrDefault();
            if (selected_city != null) { ViewBag.CityPageContent = selected_city.desc_realty; }
            if (ViewBag.CityPageContent == null) { ViewBag.CityPageContent = ""; }

            ViewBag.Message = "Top " + Capitalize(city) + ", " + prov.ToUpperInvariant() + " Real Estate Agents - Ratings and Reviews";

            var agents = db.Agents.Where(s => (s.city.Contains(city)) & (s.agent_type.Contains("Real")));
            agents = agents.OrderByDescending(s => s.cumulative_rating);

           
            int pageNumber = (page ?? 1);

            return View(agents.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Agents(string id)
        {
            ViewBag.City = id;
            return View();
        }

      

    }
}