﻿using PagedList;
using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers
{
    public class SearchController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
        int pageSize = 12;

        //
        // GET: /Search/
        public ActionResult Index(string param, string sortorder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortorder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortorder == "Date" ? "date_desc" : "Date";

            ViewBag.CurrentFilter = param;

            var agents = db.Agents.Where(s => (s.first_name.Contains(param) || s.last_name.Contains(param) || s.city.Contains(param) ));

            switch (sortorder)
            {
                case "name_desc":
                    agents = agents.OrderByDescending(s => s.first_name );
                    break;
                case "default":
                    agents = agents.OrderByDescending(s => s.cumulative_rating);
                    break;

            }


            int pageNumber = (page ?? 1);
            return View(agents.ToPagedList(pageNumber, pageSize));
           
          
        }

        // GET: /Search/Listings
        public ActionResult Listings(string param, string v, string sortorder, int? page)
        {
            ViewBag.Title = "Search Result | Agent Reviews and Ratings at Rate­-My­-Agent.com";
            ViewBag.MetaDescription = "Rate and review agents. Insurance, mortgage, and real estate agent ratings and reviews by clients and peers. Review an agent at Rate­-My­-Agent.com.";

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortorder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortorder == "Date" ? "date_desc" : "Date";

            ViewBag.Message = "Search results for '" + param + "'";

            if (param == "#") { param = ""; }
            ViewBag.CurrentFilter = param;
            if (ViewBag.CurrentFilter == "") { ViewBag.CurrentFilter = "#"; }

            ViewBag.CurrentSort = sortorder;


            var search_terms = param.Split(' ').ToList() ;
            
            List<Agent> agents = new List<Agent>();
            
            foreach (string search_term in search_terms) {

                var curr_agents = db.Agents.Where(s => (s.first_name.Contains(search_term) || s.last_name.Contains(search_term) || s.city.Contains(search_term)));

                agents = agents.Union(curr_agents).ToList();

            }

            

           var qagents = agents.AsQueryable();

            if (v == "l")
            {
                return View(qagents);
            }
            else
            {
                switch (sortorder)
                {
                    case "name_desc":
                        qagents = qagents.OrderByDescending(s => s.first_name);
                        break;
                    case "default":
                        qagents = qagents.OrderByDescending(s => s.cumulative_rating);
                        break;
                }


                int pageNumber = (page ?? 1);
                return View(qagents.ToPagedList(pageNumber, pageSize));
            }
           
        }

    }
}