﻿using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminScriptsController : Controller
    {

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
      

        // GET: AdminScripts
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult RankAgents()
        {
            var reviews = db.Reviews.ToList();
            var agents = db.Agents.ToList();

            var counter = 0;
            var progress_counter = 0;

            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = false;



            foreach (Agent curr_agent in agents)
            {
                int parsed_agent_id = -1;
                //int.TryParse(curr_agent.RMA_ID, out parsed_agent_id);
                parsed_agent_id = curr_agent.RMA_ID;

                if (parsed_agent_id > -1)
                {   
                    var review_list = reviews.Where(g => g.agent_id == parsed_agent_id).ToList();
                    

                    var total_rating_count = 0;
                    foreach (var curr_review in review_list)
                     {
                        total_rating_count += curr_review.overall_rating;
                    }
                    var count = reviews.Where(g => g.agent_id == parsed_agent_id).Count();

                    //db.Agents.Where(i => i.RMA_ID == curr_agent.RMA_ID).FirstOrDefault().total_review_count = reviews.Where(g => g.agent_id == parsed_agent_id).Count(); //curr_agent.Reviews.Count();


                    // if (total_rating > 0 & curr_agent.total_review_count > 0)
                    //  {
                    //     curr_agent.average_overall_ranking = total_rating / curr_agent.total_review_count;
                    // }
                   

                    curr_agent.total_review_count = count;
                    curr_agent.cumulative_rating = total_rating_count;
                    db.Agents.Where(i => i.RMA_ID == curr_agent.RMA_ID).FirstOrDefault().total_review_count = curr_agent.total_review_count;
                    db.Entry(curr_agent).State = EntityState.Modified;
                   //   
                  //  }

                   
                    
                }

               

               //db.Agents.Where(i => i.RMA_ID  == curr_agent.RMA_ID ).FirstOrDefault().total_review_count = curr_agent.total_review_count ;

               
                counter++;
                progress_counter++;
                if (counter == 200)
                {
                    counter = 0;

                }


            }

            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            counter = 0;
            db = new RMA_DBContext("RMA_DBContext");
          

            db.SaveChanges();


            return View();
        }


        public ActionResult RankCities()
        {
            var cities = db.Cities.ToList();
            var agents = db.Agents.ToList();

            var counter = 0;
            var progress_counter = 0;

            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = false;



            foreach (City curr_city in cities)
            {
        
             
                var count = agents.Where(g => g.city == curr_city.name).Count();

                curr_city.agent_count  = count;                
                db.Entry(curr_city).State = EntityState.Modified;
                 
                counter++;
                progress_counter++;
                if (counter == 200)
                {
                    counter = 0;

                }


            }

            db.Configuration.AutoDetectChangesEnabled = false;
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            counter = 0;
            db = new RMA_DBContext("RMA_DBContext");


            db.SaveChanges();


            return View();
        }



    }
}