﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;

namespace RMA_SITE.Controllers.Members_Arena
{
    public class ProFieldsController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: ProFields
        public ActionResult Index()
        {
            return View(db.ProFields.ToList());
        }

        // GET: ProFields/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProField proField = db.ProFields.Find(id);
            if (proField == null)
            {
                return HttpNotFound();
            }
            return View(proField);
        }

        // GET: ProFields/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProFields/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,field_type,RMA_ID,field_value")] ProField proField)
        {
            if (ModelState.IsValid)
            {
                db.ProFields.Add(proField);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proField);
        }

        // GET: ProFields/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProField proField = db.ProFields.Find(id);
            if (proField == null)
            {
                return HttpNotFound();
            }
            return View(proField);
        }

        // POST: ProFields/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,field_type,RMA_ID,field_value")] ProField proField)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proField).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proField);
        }

        // GET: ProFields/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProField proField = db.ProFields.Find(id);
            if (proField == null)
            {
                return HttpNotFound();
            }
            return View(proField);
        }

        // POST: ProFields/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProField proField = db.ProFields.Find(id);
            db.ProFields.Remove(proField);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
