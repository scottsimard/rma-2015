﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace RMA_SITE.Controllers
{
    public class ReviewsController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: Ratings
        public ActionResult Index()
        {
            return View(db.Reviews.ToList());
        }

        // GET: Ratings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review rating = db.Reviews.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        // GET: Ratings/Details/5
        public ActionResult ViewSingle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review rating = db.Reviews.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }

            ViewBag.AgentName = "AGENT NAME";

            return View(rating);
        }


        // GET: Ratings/Create
        public ActionResult Create(int? agent_id)
        {

            if (User.Identity.IsAuthenticated)
            {
                //var agent_list = db.Agents;
                //List<SelectListItem> items = new List<SelectListItem>();
                //foreach (var t in agent_list)
                //{
                //    SelectListItem s = new SelectListItem();
                //    s.Text = t.first_name + " " + t.last_name;
                //    s.Value = Convert.ToString(t.ID);
                //    if (agent_id != null)
                //    {
                //        if (agent_id == t.ID)
                //        {
                //            s.Selected = true;
                //        }
                //    }

                //    items.Add(s);
                //}

                //ViewBag.agent_list = items;
                //if (agent_id == null)
                //{
                //    agent_id = 0;
                //}
                //ViewBag.agent_id = agent_id;

                return View();
            }
            else
            {
                ViewBag.LoggedInMessage = "You must be logged in to write a review.";
                return View("MustBeLoggedIn");

            }

           
        }

        public ActionResult Received(Review review)
        {

            return View(review);
        }


        // POST: Reviews1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] Review review)
        {
            if (ModelState.IsValid)
            {
                if(review.friendliness_rating == null) { review.friendliness_rating = 0; }
                if (review.attitude_rating == null) { review.attitude_rating = 0; }
                if (review.experience_rating == null) { review.experience_rating = 0; }
                if (review.handelling_claims_rating == null) { review.handelling_claims_rating = 0; }
                if (review.helpfulness_rating == null) { review.helpfulness_rating = 0; }
                if (review.mortgage_guidance_rating == null) { review.mortgage_guidance_rating = 0; }
                if (review.receive_review_notification  == null) { review.receive_review_notification = ""; }
                if (review.review_summary  == null) { review.review_summary = ""; }

                if (review.created_at  == null) { review.created_at = DateTime.Now; }
                if (review.updated_at == null) { review.updated_at = DateTime.Now; }

                if (review.verification_key == null) { review.verification_key = ""; }
                if (review.verified_reply == null) { review.verified_reply = ""; }




                db.Reviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Received");
            }

            return View(review);
        }

        //// POST: Ratings/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] Review review)
        //{


        //        if (ModelState.IsValid)
        //        {
        //            db.Reviews.Add(review);

        //            try
        //            {
        //                var sql = db.Reviews.ToString();

        //                var result =  db.SaveChanges();


        //        } catch(DbEntityValidationException ex)
        //            {
        //                foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
        //                {
        //                    DbEntityEntry entry = item.Entry;
        //                    string entityTypeName = entry.Entity.GetType().Name;

        //                    // Display or log error messages

        //                    foreach (DbValidationError subItem in item.ValidationErrors)
        //                    {
        //                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
        //                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
        //                        Console.WriteLine(message);
        //                    }
        //                }

        //            string error_message = ex.InnerException.InnerException.ToString();
        //                return RedirectToAction("Index", "Error", new { error_message = error_message.Substring(0, 350) });
        //            }

        //            return RedirectToAction("Received", new { review = review });
        //        }

        //        var agent_list = db.Agents;

        //        List<SelectListItem> items = new List<SelectListItem>();
        //        foreach (var t in agent_list)
        //        {
        //            SelectListItem s = new SelectListItem();
        //            s.Text = t.first_name + " " + t.last_name;
        //            s.Value = Convert.ToString(t.ID);
        //            items.Add(s);
        //        }

        //        ViewBag.agent_list = items;

        //        return View(review);


        //}

        // GET: Ratings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review rating = db.Reviews.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        // POST: Ratings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,RMA_ID,review_summary,agent_id,overall_rating,friendliness_rating,understanding_rating,professionalism_rating,experience_rating,successful,transaction_type_id,property_type_id,comment,helpfulness_rating,responsiveness_rating,attitude_rating,created_at,updated_at,verified_reply,verification_key,market_research_rating,home_prep_rating,handelling_claims_rating,mortgage_guidance_rating,website_rating,service_rating,show_home,recommend_agent,name,email,receive_review_notification")] Review rating)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rating).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rating);
        }

        // GET: Ratings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review rating = db.Reviews.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        // POST: Ratings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Review rating = db.Reviews.Find(id);
            db.Reviews.Remove(rating);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
