﻿using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers
{
    public class AgentIndicatorController : Controller
    {

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: AgentIndicator
        public ActionResult Index(Agent curr_agent)

        {
            if (curr_agent == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(curr_agent.ID);
            if (agent == null)
            {
                return HttpNotFound();
            }

            return PartialView(agent);
        }

      

    }
}