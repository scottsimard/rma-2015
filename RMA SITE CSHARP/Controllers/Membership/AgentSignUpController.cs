﻿using RMA_SITE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers.Membership
{
    public class AgentSignUpController : Controller
    {

        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");

        // GET: AgentSignUp
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Charge(string stripeToken, string stripeEmail)
        {
            string apiKey = "sk_test_kyZdLxoHiSjLrltdjSNx6pir";
            var stripeClient = new Stripe.StripeClient(apiKey);
            

            dynamic response = stripeClient.CreateChargeWithToken(9.99m, stripeToken, "CAD", "NEW USER - " + stripeEmail);

            if (response.IsError == false && response.Paid)
            {
                db = new RMA_DBContext("RMA_DBContext");
                
                Agent agent = db.Agents.FirstOrDefault(i => i.email == User.Identity.Name );
                if(agent != null)
                {
                    agent.pro_member = true;
                    db.SaveChanges();
                }
                
                return View("ChargeComplete");
            }

            ViewBag.ErrorMessage = "Error processing payment. Try again later.";

            return View("Index");

        }

    }



}