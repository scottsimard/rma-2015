﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMA_SITE.Models;
using System.IO;

namespace RMA_SITE.Controllers
{
    public class AgentsController : Controller
    {
        private RMA_DBContext db = new RMA_DBContext("RMA_DBContext");
        

        string Capitalize(string source_string)
        {
            if (source_string != null)
            {
                if (source_string.Length > 1)
                {
                    return source_string.ToUpper().Substring(0, 1) + source_string.ToLower().Substring(1);
                }
                else
                {
                    return source_string.ToUpper();
                }
            }
            else
            {
                return "";
            }
        }

        // GET: Agents
        public ActionResult Index()
        {
            return View(db.Agents.ToList());
        }

        // GET: Agents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // GET: Agents/Create
        public ActionResult Create()
        {

            var agentTypes = db.AgentTypes;
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var t in agentTypes)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.type_name;
                s.Value = t.type_name;
                items.Add( s);
            }
            
            ViewBag.agent_type_list = items;

                      
            return View();
        }

           // GET: Agents/ViewProfile/5
        public ActionResult ViewProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }

            // load agent ratings to ViewBag

            var view_model = new ViewModelAgent();
           // view_model.Reviews = db.Reviews.Include("Reviews").Single(g => g.agent_id == agent.ID));

            var reviews_list = db.Reviews.Where(g => g.agent_id == agent.ID);
            view_model.Reviews = reviews_list.ToList();
            view_model.Agent = agent;

            return View(view_model);
        }

        // GET: Agents/Profile/5
        public ActionResult Profiles(string agent_id)
        {
            if (agent_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Agent agent = db.Agents.Where(g => (g.RMA_ID.ToString() == agent_id)).SingleOrDefault();

            if (agent == null)
            {
                return HttpNotFound();
            }

            // load agent ratings to ViewBag

            string agent_type_tag = "";
            switch (agent.agent_type)
            {
                case "Insurance":
                case "Real Estate":
                    agent_type_tag = "Agent";
                    break;
                case "Mortgage":
                    agent_type_tag = "Broker";
                    break;
            }

            ViewBag.Title = Capitalize(agent.first_name) + " " + Capitalize(agent.last_name) + ", " + agent.agent_type + " " + agent_type_tag + ", Ratings and Reviews, " + Capitalize(agent.city) + ", " + agent.prov_state.ToUpper();
            @ViewBag.MetaDescription = "Ratings and reviews of " + agent.agent_type + " " + agent_type_tag + " " + Capitalize(agent.first_name) + " " + Capitalize(agent.last_name) + " representing clients in " + Capitalize(agent.city) + ". Review " +
                                                    Capitalize(agent.first_name) + " " + Capitalize(agent.last_name) + " at Rate-My-Agent.com";



            var view_model = new ViewModelAgent();

           // view_model.Reviews = db.Reviews.Include("Reviews").Single(g => g.agent_id == agent.ID));


           // view_model.Reviews = db.Reviews.Include("Reviews").Where(g => g.agent_id == agent.RMA_ID).ToList();

            int agent_rma_id;
            //int.TryParse(agent.RMA_ID, out agent_rma_id);
            agent_rma_id = agent.RMA_ID;


            var reviews_list = db.Reviews.Where(g => g.agent_id == agent_rma_id);
            view_model.Reviews = reviews_list.ToList();

            view_model.Reviews = view_model.Reviews.OrderByDescending(s => s.created_at).ToList();

            view_model.ReviewsSummary = get_reviews_summary(view_model.Reviews);

            view_model.Agent = agent;

            return View(view_model);
        }

        private ReviewSumamry get_reviews_summary(List<Review> reviews)
        {
            ReviewSumamry t_review_summary = new ReviewSumamry();

            if(reviews.Count>0)
            {
                foreach(Review t_review in reviews)
                {
                    t_review_summary.understanding_rating += t_review.understanding_rating;
                    t_review_summary.overall_rating += t_review.overall_rating;
                    
                    t_review_summary.professionalism_rating += t_review.professionalism_rating;
                    t_review_summary.responsiveness_rating += t_review.responsiveness_rating;
                    t_review_summary.website_rating += t_review.website_rating;
                    t_review_summary.service_rating += t_review.service_rating;
                    t_review_summary.market_research_rating += t_review.market_research_rating;
                    t_review_summary.home_prep_rating += t_review.home_prep_rating;

                    t_review_summary.success_ratio += t_review.successful;
                }

                int review_count = reviews.Count;

                t_review_summary.overall_rating /= review_count;
                t_review_summary.understanding_rating /= review_count;
                t_review_summary.professionalism_rating /= review_count;
                t_review_summary.responsiveness_rating /= review_count;
                t_review_summary.website_rating /= review_count;
                t_review_summary.service_rating /= review_count;
                t_review_summary.market_research_rating /= review_count;
                t_review_summary.home_prep_rating /= review_count;

                t_review_summary.success_ratio = (t_review_summary.success_ratio /= review_count) * 100;

            }



            return t_review_summary;

        }



        // OLD
        //// GET: Agents/Profile/5
        //public ActionResult Profiles(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    string[] names = id.Split('-');
        //    string fname = "";
        //    string lname = "";


        //    if (names.Count() > 0)
        //    {
        //        fname = names[0];
        //        if (names.Count() >= 1)
        //        {
        //            lname = names[1];
        //        }
        //    }

        //    Agent agent = db.Agents.Where(g => ((g.first_name == fname) & (g.last_name == lname))).SingleOrDefault();

        //    if (agent == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    // load agent ratings to ViewBag

        //    string agent_type_tag = "";
        //    switch (agent.agent_type)
        //    {
        //        case "Insurance":
        //        case "Real Estate":
        //            agent_type_tag = "Agent";
        //            break;
        //        case "Mortgage":
        //            agent_type_tag = "Broker";
        //            break;
        //    }

        //    ViewBag.Title = Capitalize(agent.first_name) + " " + Capitalize(agent.last_name) + ", " + agent.agent_type + " " + agent_type_tag + ", Ratings and Reviews, " + Capitalize(agent.city) + ", " + agent.prov_state.ToUpper();


        //    var view_model = new ViewModelAgent();
        //    // view_model.Reviews = db.Reviews.Include("Reviews").Single(g => g.agent_id == agent.ID));

        //    int agent_rma_id;
        //    int.TryParse(agent.RMA_ID, out agent_rma_id);

        //    var reviews_list = db.Reviews.Where(g => g.agent_id == agent_rma_id);
        //    view_model.Reviews = reviews_list.ToList();
        //    view_model.Agent = agent;

        //    return View(view_model);
        //}




        // POST: Agents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,photo,agent_type,link,first_name,last_name,phone_number,cell_number,fax_number,address1,address2,city,prov_state,country,email,web_site,created_at,updated_at,average_overall_rating_cache,active")] Agent agent, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(upload.InputStream))
                    {
                        agent.photo_content = reader.ReadBytes(upload.ContentLength);
                    }
                
                }

                agent.updated_at = DateTime.Now;

                agent.RMA_ID = get_next_RMA_ID();

                db.Agents.Add(agent);
                db.SaveChanges();

               // int last_id = db.Agents.Max(item => item.ID);

                
                return RedirectToAction("Profiles", new { agent_id = agent.RMA_ID }); // db.Agents.Last().ID);
            }

            var agentTypes = db.AgentTypes;
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var t in agentTypes)
            {
                SelectListItem s = new SelectListItem();
                s.Text = t.type_name;
                s.Value = t.type_name;
                items.Add(s);
            }

            ViewBag.agent_type_list = items;

            return View(agent);
        }

        private int get_next_RMA_ID()
        {

            int next_RMA_ID = db.Agents.Max(item => item.RMA_ID);

            next_RMA_ID++;
            
            return next_RMA_ID;

        }

        // GET: Agents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // POST: Agents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,photo,agent_type,link,first_name,last_name,phone_number,cell_number,fax_number,address1,address2,city,prov_state,country,email,web_site,created_at,updated_at,average_overall_rating_cache,active")] Agent agent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(agent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(agent);
        }

        // GET: Agents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = db.Agents.Find(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // POST: Agents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Agent agent = db.Agents.Find(id);
            db.Agents.Remove(agent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
