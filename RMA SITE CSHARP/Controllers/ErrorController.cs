﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(string error_message)
        {
            ViewBag.error_message = error_message;
            return View();
        }
    }
}