﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mail.Models;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SendMail.Controllers
{
    public class SendMailerController : Controller
    {
        //
        // GET: /SendMailer/ 
        public ActionResult Index()
        {
            return View();
        }

       
        public ActionResult sendmail(MailModel _objModelMail)
        {
            if (ModelState.IsValid)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(_objModelMail.To);
                mail.From = new MailAddress(_objModelMail.From);
                mail.Subject = _objModelMail.Subject;
                string Body = _objModelMail.Body;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("the.phaze.projects", "Th3p0psh0ppe");
                smtp.EnableSsl = true;
                smtp.Send(mail);
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
    }
}