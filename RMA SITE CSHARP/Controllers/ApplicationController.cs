using System;
using System.Web.Mvc;
using RestfulRouting.Format;

namespace RMA_SITE.Controllers
{
    public abstract class ApplicationController : Controller
    {
        protected ActionResult RespondTo(Action<FormatCollection> format)
        {
            return new FormatResult(format);
        }


        public ActionResult ParseURL(String id)
        {


            return RedirectToAction("Index");
            
        }

    }
}