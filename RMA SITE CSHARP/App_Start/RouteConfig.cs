﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RMA_SITE
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(name: "Advertising", url: "advertising", defaults: new { controller = "Home", action = "Advertising" } );
            routes.MapRoute(name: "About", url: "about", defaults: new { controller = "Home", action = "About" } );
            routes.MapRoute(name: "Contact", url: "contact", defaults: new { controller = "Home", action = "Contact" } );
            routes.MapRoute(name: "Write a Review", url: "write-a-review", defaults: new { controller = "Reviews", action = "Create" });

            routes.MapRoute(name: "Real-estate", url: "real-estate", defaults: new { controller = "Home", action = "RealEstate" });
            routes.MapRoute(name: "Insurance", url: "insurance", defaults: new { controller = "Home", action = "Insurance" });
            routes.MapRoute(name: "Mortgage", url: "mortgage", defaults: new { controller = "Home", action = "Mortgage" });

            
            routes.MapRoute(name: "Agents", url: "{id}-ratings-{city}-{agent_id}", defaults: new { controller = "Agents", action = "Profiles", id = "{id}", city="city", agent_id="{agent_id}" });

            
            routes.MapRoute(name: "Cities-Real-Estate", url: "{city}-{prov}-real-estate-agent-ratings", defaults: new { controller = "City", action = "RealEstate", city = "{city}", prov = "prov" });
            routes.MapRoute(name: "Cities-Insurance", url: "{city}-{prov}-insurance-agent-reviews-ratings", defaults: new { controller = "City", action = "Insurance", city = "{city}", prov = "prov" });
            routes.MapRoute(name: "Cities-Mortgage", url: "{city}-{prov}-mortgage-broker-reviews-ratings", defaults: new { controller = "City", action = "Mortgage", city = "{city}", prov = "prov" });
            routes.MapRoute(name: "Cities", url: "{city}-{prov}-agent-ratings", defaults: new { controller = "City", action = "Index", city = "{city}", prov = "prov" });


            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional } );


           // routes.MapRoute(name: "Catch-All", url: "{id}", defaults: new { controller = "Application", action = "ParseURL", id = "{id}" });


        }
    }
}
