﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class ProField
    {

        public int ID { get; set; }

        [Required]
        public ProFieldType field_type { get; set; }

        [Required]
        public int RMA_ID { get; set; }

        [Required]
        public string field_value { get; set; }


    }

    public enum ProFieldType
    {
        CITY, DIVISION, SPECIALTY
    }


}