﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMA_SITE.Models
{
    public class City
    {
        public string ID { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public string name { get; set; }
        public string prov_state { get; set; }

        [AllowHtml]
        public string description { get; set; }

        [AllowHtml]
        public string desc_realty { get; set; }

        [AllowHtml]
        public string desc_mortgage { get; set; }

        [AllowHtml]
        public string desc_insurance { get; set; }

        public int agent_count { get; set; }

    }
}