﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class ViewModelAgent
    {
        public Agent Agent { get; set; }
        public List<Review> Reviews { get; set; } 

        public ReviewSumamry ReviewsSummary { get; set; }

    }
}