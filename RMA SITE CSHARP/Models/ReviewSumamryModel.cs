﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class ReviewSumamry
    {

        [DisplayName("Overall Rating")]
        public double overall_rating { get; set; }

        [DisplayName("Knowledgeability Rating")]
        public double understanding_rating { get; set; }

        [DisplayName("Professionalism Rating")]
        public double professionalism_rating { get; set; }

        [DisplayName("Responsiveness Rating")]
        public double responsiveness_rating { get; set; }

        [DisplayName("Usefulness of Website")]
        public double website_rating { get; set; }

        [DisplayName("Value of Service")]
        public double service_rating { get; set; }

        [DisplayName("Marketing Reach & Lead Generation")]
        public double market_research_rating { get; set; }

        [DisplayName("Home Prep & Staging Advice")]
        public double home_prep_rating { get; set; }

        [DisplayName("Success Ratio")]
        public double success_ratio { get; set; }

    }
}