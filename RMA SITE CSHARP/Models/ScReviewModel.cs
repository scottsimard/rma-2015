﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class ScReviewModel
    {

        public int id { get; set; }
        public int RMA_ID { get; set; }

        [DisplayName("Summary")]
        public string review_summary { get; set; }

        [DisplayName("Agent ID")]
        public int? agent_id { get; set; }

        [DisplayName("Overall")]
        public int overall_rating { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [DisplayName("Friendliness")]
        public System.Nullable<int> friendliness_rating { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [DisplayName("Understanding")]
        public System.Nullable<int> understanding_rating { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [DisplayName("Professionalism")]
        public System.Nullable<int> professionalism_rating { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [DisplayName("Experience")]
        public System.Nullable<int> experience_rating { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [DisplayName("Successful")]
        public System.Nullable<int> successful { get; set; }


        public int transaction_type_id { get; set; }

        public int property_type_id { get; set; }

        public string comment { get; set; }

        [DisplayName("Helpfulness")]
        public int? helpfulness_rating { get; set; }

        [DisplayName("Responsiveness")]
        public int? responsiveness_rating { get; set; }

        [DisplayName("Attitude")]
        public int? attitude_rating { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? updated_at { get; set; }

        public string verified_reply { get; set; }
        public string verification_key { get; set; }

        public int? market_research_rating { get; set; }


        public int? home_prep_rating { get; set; }
        public int? handelling_claims_rating { get; set; }
        public int? mortgage_guidance_rating { get; set; }
        public int? website_rating { get; set; }
        public int? service_rating { get; set; }


        public string show_home { get; set; }
        public string recommend_agent { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public string email { get; set; }
        public string receive_review_notification { get; set; }

    }
}