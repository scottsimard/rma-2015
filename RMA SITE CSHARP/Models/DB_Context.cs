﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class RMA_DBContext : DbContext
    {

        public RMA_DBContext(string connString) : base(nameOrConnectionString: connString) {

            this.Database.Connection.ConnectionString = "Data Source=SQL5016.Smarterasp.net;Initial Catalog=DB_9D00EB_rma;User Id=DB_9D00EB_rma_admin;Password=c0mputer;Integrated Security=False;";
        }

        public DbSet<Agent> Agents { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<AgentType> AgentTypes { get; set; }
        public DbSet<City> Cities { get; set; }

        public DbSet<ProField> ProFields { get; set; }

        public DbSet<ScReviewModel> ScReviews { get; set; }
        


        public System.Data.Entity.DbSet<RMA_SITE.Models.DataImporterModel> DataImporterModels { get; set; }


        public override int SaveChanges()
        {
            try
            {

                return base.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch(System.Data.Entity.Infrastructure.DbUpdateException ex2)
            {
                var error = ex2.InnerException.ToString();
                return 0;
            }
        }

    }



}