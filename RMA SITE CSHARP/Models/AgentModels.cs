﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RMA_SITE.Models
{
    public class Agent
    {
        public Agent()
        {
            created_at = DateTime.Now;
           

        }

        public int ID { get; set; }

        [Required]
        [DisplayName("Agent ID")]
        public int RMA_ID { get; set; }

        public byte[] photo_content { get; set; }

        [DisplayName("Type")]
        public string agent_type { get; set; }

        [Required]
        [DisplayName("First Name")]
        public string first_name { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public string last_name { get; set; }

        [DisplayName("Phone Number")]
        public string phone_number { get; set; }

        [DisplayName("Cell Number")]
        public string cell_number { get; set; }

        [DisplayName("Fax Number")]
        public string fax_number { get; set; }

        [DisplayName("Address 1")]
        public string address1 { get; set; }

        [DisplayName("Address 2")]
        public string address2 { get; set; }

        [Required]
        [DisplayName("City")]
        public string city { get; set; }

        [Required]
        [DisplayName("Prov / State")]
        public string prov_state { get; set; }

        [Required]
        [DisplayName("Country")]
        public string country { get; set; }


        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Website Address")]
        public string web_site { get; set; }

        [DisplayName("Company Name")]
        public string company_name { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public float  average_overall_rating_cache { get; set; }

        public float cumulative_rating { get; set; }
        
        public float average_overall_ranking { get; set; }
        public int total_review_count { get; set; }


        public int active { get; set; }

        public bool pro_member { get; set; }

        public string photo_filename { get; set; }
        public string photo_link { get; set; }
        public string photo_path {
                get
                {                
                    if (photo_link != "")
                    {
                        return photo_link;
                    }
                    else
                    {
                        if (photo_filename != "")
                        {
                            return "../Content/Images/Agents/" + photo_filename;
                        }
                        else
                        {
                        return "../Content/Images/Agents/unknown.jpg";
                        }
                    }

                }
        }

      //  public List<Review> Reviews { get; set; }

        

    }




}