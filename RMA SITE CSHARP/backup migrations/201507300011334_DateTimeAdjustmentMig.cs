namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeAdjustmentMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Agents", "created_at", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Agents", "updated_at", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Agents", "updated_at", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Agents", "created_at", c => c.DateTime(nullable: false));
        }
    }
}
