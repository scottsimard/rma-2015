namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesToPhotoInAgentsMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agents", "photo_content", c => c.Binary());
            AddColumn("dbo.Agents", "photo_filename", c => c.String());
            DropColumn("dbo.Agents", "photo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Agents", "photo", c => c.Binary());
            DropColumn("dbo.Agents", "photo_filename");
            DropColumn("dbo.Agents", "photo_content");
        }
    }
}
