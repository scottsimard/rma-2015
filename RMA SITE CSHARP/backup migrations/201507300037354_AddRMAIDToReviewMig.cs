namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRMAIDToReviewMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "RMA_ID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "RMA_ID");
        }
    }
}
