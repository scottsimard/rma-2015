namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSummaryToRatingMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "rating_summary", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "rating_summary");
        }
    }
}
