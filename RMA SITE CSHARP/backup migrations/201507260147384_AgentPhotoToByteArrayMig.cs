namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgentPhotoToByteArrayMig : DbMigration
    {
        public override void Up()
        {
    
            AddColumn("dbo.Agents", "ColumnNameTmp", c => c.Binary());
            Sql("Update dbo.Agents SET ColumnNameTmp = Convert(varbinary, photo)");
            DropColumn("dbo.Agents", "photo");
            RenameColumn("dbo.Agents", "ColumnNameTmp", "photo");

        }
        
        public override void Down()
        {
            
            
        }
    }
}
