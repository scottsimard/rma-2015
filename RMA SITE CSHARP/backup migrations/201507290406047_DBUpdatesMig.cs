namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DBUpdatesMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataImporterModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DataImporterModels");
        }
    }
}
