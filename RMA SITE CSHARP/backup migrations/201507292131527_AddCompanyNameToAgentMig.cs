namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyNameToAgentMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agents", "company_name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Agents", "company_name");
        }
    }
}
