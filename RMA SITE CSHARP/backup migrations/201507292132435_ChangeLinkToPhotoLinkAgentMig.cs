namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLinkToPhotoLinkAgentMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agents", "photo_link", c => c.String());
            DropColumn("dbo.Agents", "link");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Agents", "link", c => c.String());
            DropColumn("dbo.Agents", "photo_link");
        }
    }
}
