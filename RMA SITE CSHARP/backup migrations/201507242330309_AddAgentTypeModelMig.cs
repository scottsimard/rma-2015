namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAgentTypeModelMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgentTypes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        type_name = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AgentTypes");
        }
    }
}
