namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRMAIDToAgentMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agents", "RMA_ID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Agents", "RMA_ID");
        }
    }
}
