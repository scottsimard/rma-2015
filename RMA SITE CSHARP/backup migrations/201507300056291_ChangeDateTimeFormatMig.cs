namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDateTimeFormatMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "created_at", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Reviews", "updated_at", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "updated_at", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Reviews", "created_at", c => c.DateTime(nullable: false));
        }
    }
}
