namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRevewiModelMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "successful", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "transaction_type_id", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "property_type_id", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "comment", c => c.String());
            AddColumn("dbo.Reviews", "verified_reply", c => c.String());
            AddColumn("dbo.Reviews", "verification_key", c => c.String());
            AddColumn("dbo.Reviews", "market_research_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "home_prep_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "handelling_claims_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "mortgage_guidance_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "website_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "service_rating", c => c.Int(nullable: false));
            AddColumn("dbo.Reviews", "show_home", c => c.String());
            AddColumn("dbo.Reviews", "recommend_agent", c => c.String());
            AddColumn("dbo.Reviews", "name", c => c.String());
            AddColumn("dbo.Reviews", "email", c => c.String());
            AddColumn("dbo.Reviews", "receive_review_notification", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "receive_review_notification");
            DropColumn("dbo.Reviews", "email");
            DropColumn("dbo.Reviews", "name");
            DropColumn("dbo.Reviews", "recommend_agent");
            DropColumn("dbo.Reviews", "show_home");
            DropColumn("dbo.Reviews", "service_rating");
            DropColumn("dbo.Reviews", "website_rating");
            DropColumn("dbo.Reviews", "mortgage_guidance_rating");
            DropColumn("dbo.Reviews", "handelling_claims_rating");
            DropColumn("dbo.Reviews", "home_prep_rating");
            DropColumn("dbo.Reviews", "market_research_rating");
            DropColumn("dbo.Reviews", "verification_key");
            DropColumn("dbo.Reviews", "verified_reply");
            DropColumn("dbo.Reviews", "comment");
            DropColumn("dbo.Reviews", "property_type_id");
            DropColumn("dbo.Reviews", "transaction_type_id");
            DropColumn("dbo.Reviews", "successful");
        }
    }
}
