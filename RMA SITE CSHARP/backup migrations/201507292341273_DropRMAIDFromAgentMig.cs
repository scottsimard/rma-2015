namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropRMAIDFromAgentMig : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Agents", "RMA_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Agents", "RMA_ID", c => c.Int(nullable: false));
        }
    }
}
