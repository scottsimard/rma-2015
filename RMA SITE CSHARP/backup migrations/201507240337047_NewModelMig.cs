namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewModelMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        photo = c.String(),
                        agent_type = c.String(),
                        link = c.String(),
                        first_name = c.String(),
                        last_name = c.String(),
                        phone_number = c.String(),
                        cell_number = c.String(),
                        fax_number = c.String(),
                        address1 = c.String(),
                        address2 = c.String(),
                        city = c.String(),
                        prov_state = c.String(),
                        country = c.String(),
                        email = c.String(),
                        web_site = c.String(),
                        created_at = c.DateTime(nullable: false),
                        updated_at = c.DateTime(nullable: false),
                        average_overall_rating_cache = c.Single(nullable: false),
                        active = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.Movies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        ReleaseDate = c.DateTime(nullable: false),
                        Genre = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rating = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.Agents");
        }
    }
}
