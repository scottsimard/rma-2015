namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedNameRatingToReviewMig : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Reviews", newName: "Reviews");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Reviews", newName: "Ratings");
        }
    }
}
