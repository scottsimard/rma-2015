namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgentTypeNameToStringMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AgentTypes", "type_name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AgentTypes", "type_name", c => c.Int(nullable: false));
        }
    }
}
