namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeReviewModelValidationsMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "review_summary", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "review_summary", c => c.String(maxLength: 255));
        }
    }
}
