namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRatingToReviewMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "review_summary", c => c.String(maxLength: 255));
            DropColumn("dbo.Reviews", "rating_summary");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "rating_summary", c => c.String(maxLength: 255));
            DropColumn("dbo.Reviews", "review_summary");
        }
    }
}
