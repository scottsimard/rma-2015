namespace RMA_SITE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRatingModelMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        agent_id = c.Int(nullable: false),
                        created_at = c.DateTime(nullable: false),
                        updated_at = c.DateTime(nullable: false),
                        overall_rating = c.Int(nullable: false),
                        friendliness_rating = c.Int(nullable: false),
                        understanding_rating = c.Int(nullable: false),
                        professionalism_rating = c.Int(nullable: false),
                        experience_rating = c.Int(nullable: false),
                        helpfulness_rating = c.Int(nullable: false),
                        responsiveness_rating = c.Int(nullable: false),
                        attitude_rating = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Reviews");
        }
    }
}
